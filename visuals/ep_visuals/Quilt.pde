
// based on 9 block quilt pattern system
// inspired by http://www.levitated.net/daily/lev9block.html


class Quilt 
{
  int quiltW; // block  size
  int outerBlock = 9;
  int innerBlock = 3;
  int letterW;
  Quilt( ) {   
  }


  // blocks that are allowed only in the central cell
  private void drawInnerBlocks( int id ) 
  {
    noStroke();
    fillRandom();
    //fill(0);
    
    pushMatrix();
    translate( -quiltW/2, -quiltW/2); // center

    switch (id) {
    case 0 : 
      rectMode(CORNER);
      rect(0, 0, quiltW, quiltW); // 1:1 square
      break;
    case 1 : 
      rectMode(CENTER);
      rect( quiltW/2, quiltW/2, quiltW/2, quiltW/2); // 1:2 square
      break;
    case 2 : 
      triangle( 0, quiltW, quiltW/2, 0,quiltW,quiltW ); // central triangle
      break;
    }

    popMatrix();

    rectMode(CORNER);
  }

  // block that are allowed outside the central cell
  private void drawOuterBlocks( int id )  {
    stroke(0);

    strokeWeight(1);
    fill(0);
    pushMatrix();
    translate( -quiltW/2, -quiltW/2); // center
    switch(id) {
    case 0 :
      rect( 0, 0,  quiltW/4, quiltW/3); 
      break;
    case 1 :
      rect(0, 0, quiltW/3, quiltW/3);
      break;
    case 2 :
      // triangle(0, 0, 0, quiltW/2, 0, quiltW/2);
      triangle( quiltW/2, quiltW, quiltW, quiltW, quiltW, quiltW/2);    // up and down tri
      break;
    case 3 :
      //        rect(0, 0,  quiltW, quiltW/(1+(int)random(10)) ); // small sharp
      triangle( 0, 0,  quiltW,0,  0, quiltW/5 ); // small sharp
      break;
    case 4 :
      strokeWeight(1);
      stroke(0);
      line(2, 2,  quiltW/2,  quiltW/2);
      line( quiltW/2, quiltW, quiltW, quiltW );    // lines up and down
      break;
    case 5 :
      ellipseMode(CENTER);
      ellipse( quiltW/2, quiltW/2, quiltW/2, quiltW/2 );    // lines up and down
      break;
    case 6 :
      beginShape();
      vertex(0, letterW);
      vertex(letterW, 0);
      vertex(letterW*2, 0);
      vertex(letterW*3, letterW);
      vertex(letterW*3, letterW*2);
      vertex(letterW*2, letterW*3);
      vertex(letterW, letterW*3);
      vertex(0, letterW*2);
      vertex(0, letterW);
      vertex(letterW, letterW);
      vertex(letterW, letterW*2);
      vertex(letterW, letterW*1.5);
      vertex(letterW*1.5, letterW*1.5);
      vertex(letterW, letterW*2);
      vertex(letterW*2, letterW*2);
      vertex(letterW*2, letterW);
      vertex(letterW, letterW);
      endShape(CLOSE);
      break;
    case 7 :
      // X
      triangle(0,0,0,letterW,letterW,letterW);// top left
      triangle(letterW*2,letterW,letterW*3,0,letterW*3,letterW); // top right
      triangle(letterW*2, letterW*2, letterW*3, letterW*2, letterW*3,letterW*3); // btm right
      triangle(0, letterW*2, letterW, letterW*2, 0, letterW*3); // btm left
      noFill();
      triangle(letterW, letterW*2, letterW*1.5, letterW*1.5, letterW*2, letterW*2); // joint btm
      triangle(letterW, letterW, letterW*1.5, letterW*1.5, letterW*2, letterW); // joint top

      break;
    case 8 :

      stroke(0);
      noFill();
      //A
      beginShape();
      vertex( 0, letterW);
      vertex( letterW, 0);
      vertex( letterW*2, 0);
      vertex(letterW*3, letterW);
      vertex(letterW*3, letterW*2);
      vertex(letterW*3, letterW*3);
      vertex(letterW*2, letterW*3);
      vertex(letterW*2, letterW*2);
      //vertex(quiltW*2, quiltW);
      vertex(letterW, letterW);
      vertex(letterW, letterW*2);
      vertex(letterW, letterW*3);
      //vertex(0, quiltW*3);
      endShape(CLOSE);
      fill(0);
      triangle( letterW, letterW*2, letterW*2, letterW, letterW*2, letterW*2); // joint middle
      triangle(0, letterW*3, 0, letterW, letterW, letterW*3 ); // left triangle
      triangle(letterW*3, letterW*3, letterW*2, letterW*3, letterW*3, letterW ); // right triangle
      break;
    }
    popMatrix();
  }



  void drawNineSquareBlock( int x, int y, int quiltWidth)
  {
    quiltW = quiltWidth/3;
    letterW = quiltW/3;
    
    pushMatrix();

    translate(x , y );

    float rot1 = floor((5)) * HALF_PI; // left
    float rot2 = floor(random(5)) * HALF_PI; // mid 

    // pick random block ids
    int c1 = floor(random(outerBlock));
    int c2 = floor(random(outerBlock));
    //int c3 = floor(random(outerBlock));
    int cc = floor(random(innerBlock));

    // top Left
    pushMatrix();
    translate( quiltW/2,  quiltW/2 );
    drawGrid();
    rotate(rot1);
    drawOuterBlocks(c1);
    popMatrix();

    // top middle
    pushMatrix();
    translate( quiltW + quiltW/2,  quiltW/2 );
    drawGrid();
    rotate(rot2);
    drawOuterBlocks(c2);
    popMatrix();

    // top right
    pushMatrix();
    translate( quiltW*2+ quiltW/2,  quiltW/2 );
    drawGrid();
    rotate(rot1+HALF_PI);
    drawOuterBlocks(c1);
    popMatrix();

    // middle left
    pushMatrix();
    translate( quiltW/2, quiltW +  quiltW/2 );
    drawGrid();
    rotate(rot2 - HALF_PI);
    drawOuterBlocks(c2);
    popMatrix();

    // middle middle
    pushMatrix();
    translate( quiltW + quiltW/2, quiltW + quiltW/2);
    drawGrid();
    drawInnerBlocks(cc);
    popMatrix();

    // middle right
    pushMatrix();
    translate(quiltW*2+ quiltW/2, quiltW+ quiltW/2); 
    drawGrid(); 
    rotate(rot2+HALF_PI);
    drawOuterBlocks(c2);
    popMatrix();

    // btm left
    pushMatrix();
    translate( quiltW/2, quiltW*2+ quiltW/2 );
    drawGrid();
    rotate(rot1-HALF_PI);
    drawOuterBlocks(c1);
    popMatrix();

    // btm middle
    pushMatrix();
    translate( quiltW+ quiltW/2, quiltW*2+ quiltW/2 );
    drawGrid();
    rotate(rot2 + PI);
    drawOuterBlocks(c2); 
    popMatrix();

    // btm right
    pushMatrix();
    translate( quiltW*2+ quiltW/2, quiltW*2+ quiltW/2 );
    drawGrid();
    rotate(rot1+PI);
    drawOuterBlocks(c1); 
    popMatrix();
    // drawContour();
    popMatrix();

  }


  // nice for debug
  void drawContour() {
    // strokeWeight(0.4f);
    noFill();
    stroke(0);
    rect( -(quiltW*3)/2, -(quiltW*3)/2 ,quiltW*3,quiltW*3);
  }


  // nice for debug
  void drawGrid(){
    //strokeWeight(0.4f);
    
    noFill();
    stroke(220);
    rect( -quiltW/2, -quiltW/2,quiltW,quiltW );
  }

  private void fillRandom(){

    float rdn = random(90);
    
    if( rdn > 0 && rdn < 30) {
      fill(50,100,random(200));
    }

    if( rdn > 30 && rdn < 60) {
      fill(random(200),50,50);
    }

    if( rdn > 60) {
      fill(random(200),random(200),50);
    }

  }

}


