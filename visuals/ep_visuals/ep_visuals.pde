import processing.pdf.*;

/// press click or space bar to regenerate random 3 x 3 backround pattern
/// press s to save a png at screen resolution
/// press p to save a pdf

int w = 1200;           // width
int h = 600;            // height
int numQuiltLayers = 1; // number of quilts layers we want to generate on top of eachother.
int startWidth = 40;    // how big the quilt cell needs to be
int pattW = startWidth;

boolean change;
boolean record;

Quilt quilt;
Oxa oxa;



void setup()
{
  background(255);
  size(w,h);
 
  quilt  = new Quilt();
  oxa    = new Oxa();
  change = true;  
}


void draw()
{
  
  if(change) 
  {
    
    
    //smooth();
    background(255);
    
    if(record){
      beginRecord(PDF, "frame-####.pdf");
    }
    
    int gaps = pattW-1;
    
    // draw pattern
    for( int i = 0 ; i < numQuiltLayers; i ++) 
    { 
      drawPattern( quilt, pattW, gaps);
      gaps   /=  2;
      pattW  /= 2;   
    }
    pushMatrix();
    noSmooth();
    
    // draw 0Xa
    translate( 0, startWidth*2);
    oxa.draw(startWidth);
    popMatrix();
    
    change = false;
    
    if(record){
      endRecord();
      record = false;
    }

  }
}
  


void drawPattern( Quilt pattern, int blockWidth, int gaps){ 
  pushMatrix();
  for( int x = 0 ;  x <= width ; x += gaps)  {
    pushMatrix();
    for( int y = 0 ; y < height ;  y += gaps) {
      pushMatrix();
      pattern.drawNineSquareBlock(x, y , blockWidth);
      popMatrix();
    }
    popMatrix();
  }
  popMatrix();
}


void mouseReleased(){
  regenerate();
}


// regenerate pattern
void regenerate(){
    println(" ~~~~ regenerate");
    pattW = startWidth;
    change = true;
}


// save an image
void keyPressed() 
{  
  // save png at screen resolution
  if(key == 's' || key == 'S') {
     String imgNum = String.valueOf(year())+"."+String.valueOf(month())+"."+String.valueOf(day())+"_"+String.valueOf(hour())+"_"+String.valueOf(minute())+"_"+String.valueOf(second());
     save("ScreenGrab_"+imgNum+".png");
     println(" ~~~~ ScreenGrab_"+imgNum+".png saved");
  }
  
  if( key == ' ')
    regenerate();
  
  // record to pdf the current display
  if( key == 'p' || key == 'P' ) {
    println(" ~~~~ record pdf");
    record = true;
  }  
}

