
// just write 0Xa

class Oxa
{

  int blockW; // block  size
  int titleSpacing  = 4;
  
  Oxa(){
  }


  public void draw(int blockWidth )  
  {  
    // width of one cell ( we're using a 3x3 grid)
    int blockW = blockWidth/3;

    // bg
    fill(0);
    stroke(0);
    
    rect( 0, -blockW-2, width,blockW*6-2);
    strokeWeight(1);
    
    stroke(255,255,255);
    pushMatrix();
    translate(0 , -blockW*2);
    drawZigNzag(blockW/2);
    translate(0 , blockW*6 - 3 );
    drawZigNzag(blockW/2);
    popMatrix();
  
    fill(255);
    stroke(255);

    translate( blockW, 3 );

    // 0
    beginShape();
    vertex( 0, blockW);
    vertex( blockW, 0);
    vertex( blockW*2, 0);
    vertex(blockW*3, blockW);
    vertex(blockW*3, blockW*2);
    vertex(blockW*2, blockW*3);
    vertex(blockW, blockW*3);
    vertex(0, blockW*2);
    vertex(0, blockW);
    vertex(blockW, blockW);
    vertex(blockW, blockW*2);
    vertex(blockW, blockW*1.5);
    vertex(blockW*1.5, blockW*1.5);
    vertex(blockW, blockW*2);
    vertex(blockW*2, blockW*2);
    vertex(blockW*2, blockW);
    vertex(blockW, blockW);
    endShape(CLOSE);

    translate(blockW*4, 0);

    // X
    triangle(0,0,0,blockW,blockW,blockW);// top left
    triangle(blockW*2,blockW,blockW*3,0,blockW*3,blockW); // top right
    triangle(blockW*2, blockW*2, blockW*3, blockW*2, blockW*3,blockW*3); // btm right
    triangle(0, blockW*2, blockW, blockW*2, 0, blockW*3); // btm left
    noFill();
    triangle(blockW, blockW*2, blockW*1.5, blockW*1.5, blockW*2, blockW*2); // joint btm
    triangle(blockW, blockW, blockW*1.5, blockW*1.5, blockW*2, blockW); // joint top

    translate(blockW*4, 0);

    stroke(255);
    noFill();
    //A
    beginShape();
    vertex( 0, blockW);
    vertex( blockW, 0);
    vertex( blockW*2, 0);
    vertex(blockW*3, blockW);
    vertex(blockW*3, blockW*2);
    vertex(blockW*3, blockW*3);
    vertex(blockW*2, blockW*3);
    vertex(blockW*2, blockW*2);
    vertex(blockW, blockW);
    vertex(blockW, blockW*2);
    vertex(blockW, blockW*3);
    endShape(CLOSE);
    fill(255);
    triangle(blockW, blockW*2, blockW*2, blockW, blockW*2, blockW*2); // joint middle
    triangle(0, blockW*3, 0, blockW, blockW, blockW*3); // left triangle
    triangle(blockW*3, blockW*3, blockW*2, blockW*3, blockW*3, blockW); // right triangle

    translate(blockW*4+4, blockW + 4); 
    
    
    blockW /= 5;
    stroke(255);
    noFill();

    // [
    beginShape();
    vertex( blockW, blockW*3 );
    vertex( 0, blockW*3);
    vertex( 0, 0 );
    vertex( blockW, 0);
    endShape();


    //e
    translate(blockW*2+1, 0);    
    drawE(blockW);
    // x
    translate(blockW*4, 0);  
    drawX(blockW);   
    // p 
    translate(blockW*4, 0);  
    drawP( blockW);
    // r  
    translate(blockW*4, 0);
    drawR(blockW);
    //~
    translate(blockW*4, 0);
    drawTilde(blockW);

    translate(blockW*2, 0);
    //]
    beginShape();
    vertex( blockW*2, blockW*3 );
    vertex( blockW*3, blockW*3);
    vertex( blockW*3, 0);
    vertex( blockW*2, 0);
    endShape();

    translate(blockW*20, 0);  

    // 1
    draw1(blockW);
    // _
    translate(blockW, 0);  
    draw_(blockW);
    // w
    translate(blockW*3, 0);  
    drawW( blockW );


    // i
    translate(blockW*3, 0);  
    drawI(blockW);
    // n
    translate(blockW*2, 0);  
    drawN(blockW);
    // t
    translate(blockW*4, 0);  
    drawT(blockW);
    // e
    translate(blockW*3, 0);      
    drawE(blockW);
    //r
    translate(blockW*4, 0);      
    drawR(blockW);
    //s
    translate(blockW*5, 0);      
    drawS(blockW);
    //o
    translate(blockW*4, 0);
    drawO(blockW);
    // l 
    translate(blockW*3, 0);
    drawL(blockW);
    translate(blockW*3, 0);
    //s
    drawS(blockW);
    translate(blockW*4, 0);
    //t
    drawT(blockW);
    translate(blockW*2, 0);
    // i
    drawI( blockW );
   //c
    translate(blockW*2, 0);
    drawC(blockW);
   // e
    translate(blockW*3, 0);
    drawE(blockW);


    //~
    translate(blockW*titleSpacing, 0);
    drawTilde(blockW);    
    // 2
    translate(blockW*4, 0);
    draw2(blockW);

    translate(blockW*3, 0);
    //_
    draw_(blockW);
    translate(blockW*3, 0);
    //0
    draw0(blockW);
    translate(blockW*4, 0);
    //9
    draw9(blockW);
    translate(blockW*4, 0);
    //0
    draw0(blockW);
    // 4
    translate(blockW*4, 0);
    draw4(blockW);
    //-
    translate( blockW*4, 0);
    drawMinus(blockW);
    // 4
    translate(blockW*3, 0);
    draw4(blockW);
    


    //~
    translate(blockW*titleSpacing, 0);
    drawTilde(blockW);
    // 3
    translate(blockW*titleSpacing, 0);
    draw3(blockW);
    // _
    translate(blockW*3, 0);
    draw_(blockW);
    // n
    translate(blockW*3, 0);
    drawN(blockW);
    //a
    translate(blockW*4, 0);
    drawA(blockW);
    // n
    translate(blockW*4, 0);
    drawN(blockW);
    //d
    translate(blockW*4, 0);
    drawD(blockW);
    //s
    translate(blockW*4, 0);
    drawS(blockW);
    //s
    translate(blockW*4, 0);
    drawY(blockW);
    //n
    translate(blockW*4, 0);
    drawN(blockW);
    //n
    translate(blockW*4, 0);
    drawT(blockW);
    //h
    translate( blockW*3, 0 );
    drawH( blockW );



    //~
    translate(blockW*titleSpacing, 0);
    drawTilde(blockW);
    // 4
    translate(blockW*titleSpacing, 0);
    draw4(blockW);
    // _
    translate(blockW*3, 0);
    draw_(blockW);
    // m
    translate(blockW*3, 0);
    drawM(blockW);
    // o
    translate(blockW*4, 0);
    drawO(blockW);
    // v
    translate(blockW*4, 0);
    drawV(blockW);
    // i
    translate(blockW*3, 0);
    drawI(blockW);
    // n
    translate(blockW*2, 0);
    drawN(blockW);
    // g
    translate(blockW*4, 0);
    drawG(blockW);
    // 
    translate(blockW*1, 0);
    // s
    translate(blockW*4, 0);
    drawS(blockW);  
    // o
    translate(blockW*4, 0);
    drawO(blockW); 
    // u
    translate(blockW*4, 0);
    drawU(blockW); 
    // f
    translate(blockW*4, 0);
    drawF(blockW);


    //~
    translate(blockW*titleSpacing, 0);
    drawTilde(blockW);
    // 5
    translate(blockW*titleSpacing, 0);
    draw5(blockW);
    // _
    translate(blockW*3, 0);
    draw_(blockW);
    // k
    translate(blockW*3, 0);
    drawK(blockW);
    // u
    translate(blockW*4, 0);
    drawU(blockW);
    // r
    translate(blockW*4, 0);
    drawR(blockW);
    // o
    translate(blockW*4, 0);
    drawO(blockW);
    // s
    translate(blockW*4, 0);
    drawS(blockW);
    // h
    translate(blockW*4, 0);
    drawH(blockW);
    // i
    translate(blockW*3, 0);
    drawI(blockW);
    // o
    translate(blockW*2, 0);
    drawO(blockW);


    //~
    translate(blockW*titleSpacing, 0);
    drawTilde(blockW);
    // 6
    translate(blockW*titleSpacing, 0);
    draw6(blockW);
    // _
    translate(blockW*3, 0);
    draw_(blockW);
    // f
    translate(blockW*3, 0);
    drawF(blockW);
    // a
    translate(blockW*4, 0);
    drawA(blockW);
    // m
    translate(blockW*4, 0);
    drawM(blockW);
    // i
    translate(blockW*3, 0);
    drawI(blockW);
    // l
    translate(blockW, 0);
    drawL(blockW);
    // y
    translate(blockW*3, 0);
   drawY(blockW);
   
   translate(blockW*2, 0);
   
    // m
    translate(blockW*3, 0);
    drawM(blockW);
    // a
    translate(blockW*4, 0);
    drawA(blockW);
    // r
    translate(blockW*4, 0);
    drawR(blockW);
    // t
    translate(blockW*4, 0);
    drawT(blockW);
  }

  void draw6( int blockW  ){
    beginShape();
    vertex( 0, blockW*2 );
    vertex( blockW*3, blockW*2 );
    vertex( blockW*3, blockW*3 );
    vertex( 0, blockW*3);
    vertex( 0, 0 );
    vertex( blockW*3, 0 );
    endShape();
  }
  
  
  void drawTilde( int blockW ){   
    beginShape();
    vertex( 0, blockW*2 );
    vertex( blockW, blockW);
    vertex( blockW*2, blockW*2);
    vertex( blockW*3, blockW);
    endShape(); 
  }

  void drawK( int blockW  ){
    beginShape();
    vertex( 0, 0 );
    vertex( 0, blockW*1.5 );
    vertex( blockW*3, 0 );
    endShape();
    beginShape();
    vertex(  0,  blockW*3 );
    vertex(0, blockW*1.5 );
    vertex( blockW *3, blockW *3);
    endShape();     
  }
  
void draw1( int blockW ){
   beginShape();
    vertex( 0, 0 );
    vertex( blockW, 0);
    vertex( blockW, blockW*3);
    endShape(); 
}

  void draw5( int blockW  ){
    beginShape();
    vertex( blockW*3, 0  );
    vertex( 0, 0 );
    vertex( 0, blockW );
    vertex( blockW*3,  blockW );
    vertex( blockW*3, blockW*3 );
    vertex( 0, blockW *3);
    endShape();     
  }

  void drawF( int blockW ){
    beginShape();
    vertex( 0, blockW*3 );
    vertex( 0, 0 );
    vertex( blockW*3, 0 );
    endShape(); 
    beginShape();
    vertex( 0, blockW );
    vertex( blockW*2, blockW );
    endShape();     
  }

  void drawU( int blockW ){
    beginShape();
    vertex( 0, 0 );
    vertex( 0, blockW*3 );
    vertex( blockW*3,  blockW*3);
    vertex( blockW*3, 0 );
    endShape();     
  }

  void draw2( int blockW ){
    beginShape();
    vertex( 0, blockW*0.5 );
    vertex( 0, 0 );
    vertex( blockW*3,  0 );    
    vertex( blockW*3,  blockW);
    vertex( 0 ,blockW*3 );
    vertex( blockW*3, blockW*3);
    endShape();     
  }

  void drawX( int blockW ){
    line(0,0, blockW*3,blockW*3);
    line(blockW*3,0, 0,blockW*3);
  }

  void drawC( int blockW ){
    beginShape();
    vertex( blockW*2, 0 );
    vertex( 0, 0 );
    vertex( 0, blockW*3);
    vertex( blockW*2, blockW*3);
    endShape(); 
  }

  void drawG(int blockW ){
    beginShape();
    vertex( blockW*2, blockW *1.5);
    vertex( 0, blockW *1.5 );
    vertex( 0, 0 );
    vertex( blockW*2, 0 );
    vertex(  blockW*2, blockW*3);
    vertex(  0, blockW*3);
    vertex(  0, blockW*2.5);
    endShape(); 
  }

  void drawP( int blockW ){
    beginShape();
    vertex( 0, 0);
    vertex( blockW*3, 0);
    vertex( blockW*3, blockW*2);
    vertex( 0, blockW *2);
    vertex( 0,  blockW*3);
    endShape(); 
  }

  void drawV(int blockW ){
    beginShape();
    vertex( 0, 0);
    vertex(  blockW*1.5, blockW*3);
    vertex( blockW*3, 0);
    endShape(); 
  }

  void drawO(int blockW ){
    beginShape();
    vertex( 0, 0);
    vertex(  blockW*3, 0);
    vertex( blockW*3, blockW*3);
    vertex( 0, blockW*3 );
    endShape(CLOSE); 
  }


  void drawM( int blockW ){
    beginShape();
    vertex( 0 , blockW*3 );
    vertex( 0 , 0 );
    vertex(  blockW*3,0 );
    vertex( blockW*3 , blockW*3 );
    endShape(); 
    beginShape();
    vertex( blockW*1.5, 0 );
    vertex( blockW*1.5, blockW );
    endShape(); 
  }

  void drawH( int blockW ){
    beginShape();
    vertex( 0 , 0 );
    vertex( 0 , blockW*3 );
    endShape(); 
    beginShape();
    vertex( 0 , blockW*2);
    vertex( blockW*3 , blockW*2);
    vertex( blockW*3 , blockW*3);
    endShape(); 
  }

  void drawY( int blockW ){
    beginShape();
    vertex( blockW*3 , blockW*3 );
    vertex( blockW*3 , 0 );
    endShape(); 

    beginShape();    
    vertex( 0, 0 );
    vertex( 0, blockW );
    vertex( blockW*3,  blockW );
    endShape(); 
  }

  void drawA( int blockW ){
    beginShape();
    vertex( blockW*3 , blockW*2 );
    vertex( 0 , blockW*2 );
    vertex( 0, blockW*3 );
    vertex( blockW*3, blockW*3 );
    vertex( blockW*3,  0 );
    vertex(  0 , 0 );
    endShape(); 
  }


  void drawD( int blockW ){
    beginShape();
    vertex( blockW*3 , blockW*2 );
    vertex( 0 , blockW*2 );
    vertex( 0, blockW*3 );
    vertex( blockW*3, blockW*3 );
    vertex( blockW*3,  0 );
    endShape(); 
  }


  void drawN( int blockW ){
    beginShape();
    vertex( 0 , blockW*3 );
    vertex(  0 , 0 );
    vertex( blockW*3,  0 );
    vertex( blockW*3, blockW*3 );
    endShape(); 
  }

  void draw0( int blockW ){
    beginShape();
    vertex(  0 , 0 );
    vertex( blockW*3 , 0 );
    vertex( blockW*3,  blockW*3 );
    vertex( 0, blockW*3 );
    endShape(CLOSE); 
    line(blockW*3,0, 0,blockW*3);
  }

  void draw3( int blockW ){
    beginShape();
    vertex( 0,  0 );
    vertex( blockW*3 , 0 );
    vertex(  blockW*3 , blockW*1.5 );
    endShape(); 
    beginShape();
    vertex( 0, blockW*1.5 );
    vertex( blockW*3, blockW*1.5 );
    vertex( blockW*3, blockW*3 );
    vertex( 0, blockW*3 );
    endShape(); 
  }


  void draw4( int blockW ){
    beginShape();
    vertex( blockW*3,  blockW*3 );
    vertex( blockW*3 , 0 );
    vertex(  0 , blockW*2 );
    vertex( blockW*3, blockW*2 );
    endShape(); 
  }

  void draw9( int blockW ){
    beginShape();
    vertex( blockW*3,  blockW*2 );
    vertex( 0,  blockW*2 );
    vertex(  0 , 0 );
    vertex( blockW*3 , 0 );
    vertex( blockW*3, blockW*3 );
    vertex( 0, blockW*3 );
    endShape(); 
  }

  void drawE( int blockW ){
    beginShape();
    vertex( blockW*3,  blockW*3 );
    vertex( 0, blockW*3 );
    vertex(  0 , 0 );
    vertex( blockW*3, 0);
    vertex( blockW*3, blockW*2);
    vertex( 0, blockW*2);
    endShape(); 
  }

  void drawI( int blockW ){
    beginShape();
    vertex( blockW, 0 );
    vertex( blockW, blockW*3);
    endShape();
  }

  void drawL( int blockW ){
    beginShape();
    vertex(  blockW*1.5, 0);
    vertex( blockW*1.5, blockW*3);
    vertex( blockW*2, blockW*3);
    endShape();
  }

  void drawR( int blockW ){ 
    beginShape();
    vertex( 0,  blockW*3);
    vertex( 0, 0);
    vertex( blockW*3, 0);
    vertex( blockW*3, blockW*2);
    vertex( blockW, blockW *2);
    vertex( blockW*3,  blockW*3);
    endShape(); 
  }

  void drawS( int blockW  ){
    beginShape();
    vertex( blockW*3, blockW*0.5);
    vertex( blockW*3, 0);
    vertex( 0, 0);
    vertex( 0, blockW*2);
    vertex( blockW*3, blockW*2);
    vertex( blockW*3, blockW*3);
    vertex( 0, blockW*3);
    endShape();
  }

  void drawT( int blockW ) {
    beginShape();
    vertex( 0, 0);
    vertex( 0, blockW*3);
    vertex( 0, blockW);
    vertex( blockW*2, blockW);
    endShape(); 
  }

  void drawW( int blockW ){
    beginShape();
    vertex( 0, 0 );
    vertex( 0, blockW*3);
    vertex( blockW*1.5, blockW*2);
    vertex( blockW*3, blockW*3);
    vertex( blockW*3, 0);    
    endShape(); 
  }

  void draw_(int blockW){
    beginShape();
    vertex( blockW, blockW *3);
    vertex( blockW*2, blockW*3);
    endShape();
  }

  void drawMinus( int blockW ){
    beginShape();
    vertex( 0,  blockW*1.5 );
    vertex( blockW*2 , blockW*1.5);
    endShape(); 
  }


  void drawZigNzag(int blockW)
  {
      for( int i=0; i < width/blockW; i++)
      {     
    beginShape();
        int xOffset = i*blockW;
        vertex( xOffset, blockW*2 );
        vertex( xOffset+blockW, blockW);
        vertex( xOffset+blockW*2, blockW*2);
        vertex( xOffset+blockW*3, blockW);      
    endShape();  
    }
  }


}




